class Subscription < ActiveRecord::Base
  belongs_to :user
  belongs_to :subscribed_to, :class_name => 'User'

  validates :user_id, presence: true
  validates :subscribed_to_id, presence: true
  validates :subscribed_to_id, uniqueness: { scope: :user_id, message: 'Unable to subscribe twice to one user' }
  validate :self_subscription

  private

  def self_subscription
    errors.add(:subscribed_to_id, 'Unable to subscribe to yourself.') if user_id == subscribed_to_id
  end
end
