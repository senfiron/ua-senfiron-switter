class Post < ActiveRecord::Base
  belongs_to :user
  #TODO: Try to change ordering to 'created_at DESC'
  default_scope -> { order('id DESC') }

  validates :text, presence: true,
            length: { minimum: 1, maximum: 140 }

  @@per_page = 10
  def self.per_page
    @@per_page
  end

  def self.page(start_id = nil)
    posts = limit(Post.per_page)
    posts = posts.where('posts.id < ?', start_id) if start_id

    have_more = ids.last < posts.last.id if posts.any?

    [posts, have_more]
  end
end
