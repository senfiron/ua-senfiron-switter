class User < ActiveRecord::Base

  default_scope -> { order(:id) }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :avatar, AvatarUploader
  validates_integrity_of  :avatar
  validates_processing_of :avatar

  has_many :posts, dependent: :destroy

  has_many :subscriptions, dependent: :destroy
  has_many :subscribed_to, through: :subscriptions

  has_many :inverse_subscriptions, class_name: 'Subscription', foreign_key: 'subscribed_to_id', dependent: :destroy
  has_many :subscribers, through: :inverse_subscriptions, source: :user

  def feed
    Post.where(user_id: subscribed_to.ids << id)
  end

  def subscribed_to?(user)
    subscriptions.exists?(subscribed_to_id: user.id) if user
  end

  def subscribe(user)
    subscriptions.build(subscribed_to_id: user.id).save if user
  end

  def unsubscribe(user)
    subscriptions.find_by(subscribed_to_id: user.id).destroy if user and subscribed_to?(user)
  end

  # Don't allow to change email w/o password
  def update_without_password(params, *options)
    params.delete(:email)
    super(params, *options)
  end
end
