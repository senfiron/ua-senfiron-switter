class PostsController < ApplicationController
  before_filter :authenticate_user!

  # Current user's feed (subscriptions + own posts)
  def index
    @user = current_user

    @posts, @show_more = @user.feed.page params[:last]

    @post = Post.new if not request.xhr?

    render template: 'users/show'
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user

    respond_to do |format|
      if @post.save
        flash[:notice] = 'Post was successfully created.'
      else
        flash[:alert] = 'Post was not created.'
      end

      format.html { redirect_to posts_path}
      format.js   {}
    end
  end

  def destroy
    @post = Post.find(params[:id])

    respond_to do |format|
      flash[:notice] =
      if @post.destroy
        'Post was successfully deleted.'
      else
        'Post was not deleted.'
      end

      format.html { redirect_to posts_path}
      format.js   {}
    end
  end

  private
    def post_params
      params.require(:post).permit(:text)
    end
end
