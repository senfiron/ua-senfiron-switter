class UsersController < ApplicationController

  # List of users
  def index
    @users = User.page params[:page]
  end

  # User's own posts only
  def show
    @user = User.find(params[:id])

    @posts, @show_more = @user.posts.page params[:last]

    @post = @user.posts.build if not request.xhr? and @user == current_user
  end

  def subscribers
    @title = 'subscribers'
    @user = User.find params[:id]
    @users = @user.subscribers.page(params[:page]).per(36)
    render 'users_grid'
  end

  def subscriptions
    @title = 'subscriptions'
    @user = User.find params[:id]
    @users = @user.subscribed_to.page(params[:page]).per(36)
    render 'users_grid'
  end
end
