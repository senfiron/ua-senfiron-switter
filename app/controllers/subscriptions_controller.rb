class SubscriptionsController < ApplicationController
  before_filter :authenticate_user!

  #TODO: handle errors

  def create
    @user = User.find(params[:id])

    flash[:notice] =
    if current_user.subscribe(@user)
      "Successfully subscribed to #{@user.email}."
    end

    respond_to do |format|
      format.html { redirect_to :back }
      format.js   {}
    end
  end

  def destroy
    @user = User.find(params[:id])

    flash[:notice] =
    if current_user.unsubscribe(@user)
        "Successfully unsubscribed from #{@user.email}."
    end

    respond_to do |format|
      format.html { redirect_to :back }
      format.js   {}
    end
  end
end
