class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :configure_permitted_parameters, if: :devise_controller?

  after_filter :xhr_flash_discard

  rescue_from ActionController::InvalidAuthenticityToken, with: :redirect_to_login

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) << :avatar << :avatar_cache << :remove_avatar
  end

  def xhr_flash_discard
    return unless request.xhr?
    flash.discard  # don't want the flash to appear when you reload page
  end

  def redirect_to_login
    if not user_signed_in?
      respond_to do |format|
        format.html { redirect_to new_user_session_path }
        format.js { render js: "window.location = '#{new_user_session_path}'" }
      end
      return false
    end
  end
end
