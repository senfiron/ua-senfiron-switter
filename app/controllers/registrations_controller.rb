class RegistrationsController < Devise::RegistrationsController
  def create
    super do |user|
      verify_recaptcha(model: user, message: 'There was an error with the recaptcha code below. Please re-enter the code.')
      flash.delete :recaptcha_error
    end
  end

  def update_resource(user, params)
    if user.email == params[:email] and params[:password].blank?
      params.delete(:current_password)
      user.update_without_password(params)
    else
      user.update_with_password(params)
    end
  end
end
