# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

recount = ->
  len = $('#post-input').val().length
  $('#counter').html(140 - len)
  $('#post-submit').prop('disabled', len > 140 || len < 1)

ready = ->
  $('#post-input').on('input', recount)

$(document).ready(ready);
$(document).on('page:load', ready);
