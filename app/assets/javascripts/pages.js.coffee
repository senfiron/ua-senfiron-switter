moveLogo = ->
    width = $(window).width()
    t = 100
    left = Math.round((width - 577) / (2 * t)) * t
    left = 0 if left < 0
    $('#switter').css('left',left)

ready = ->
  $(window).resize -> moveLogo()
  moveLogo()

$(document).ready(ready);
$(document).on('page:load', ready);