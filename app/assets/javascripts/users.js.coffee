# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

initTooltips = ->
  $('[data-toggle="tooltip"]').tooltip container: 'body'

ready = ->
  $('[data-toggle="popover"]').popover content: 'Loading...'
  .on 'shown.bs.popover', ->
    # Init tooltips inside popover
    initTooltips()


  $('a[data-toggle="popover"]').on 'ajax:success', ->
    # Load popover content only once
    $(this).on 'ajax:before', ->
      false
  .click ->
    # Leave only one popover on page
    $('a[data-toggle="popover"]').not(this).popover('hide')
  $('#show-more').on 'ajax:success', ->
    initTooltips()
  initTooltips()


$(document).ready(ready);
$(document).on('page:load', ready);

