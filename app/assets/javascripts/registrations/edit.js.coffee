ready = ->
  $('#myTab a').click = (e) ->
    e.preventDefault()
    $(this).tab('show')
  $('[data-toggle="tooltip"]').tooltip()

$(document).ready(ready);
$(document).on('page:load', ready);
