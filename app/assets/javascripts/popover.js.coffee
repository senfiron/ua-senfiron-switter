# Small extension to popover class to be more friendly with dynamic content
class SwitterPopover extends $.fn.popover.Constructor
  # A part of popover.show() method to update popover content and position
  setContent: (content) ->
    if content
      this.options.content = content;
      this.setContent();
      this.$tip.addClass(this.options.placement);

      pos          = this.getPosition();
      actualWidth  = this.$tip[0].offsetWidth;
      actualHeight = this.$tip[0].offsetHeight;
      placement    = this.options.placement;

      calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight);
      this.applyPlacement(calculatedOffset, placement);
      this.$element.trigger('shown.bs.' + this.type)
    else super

ready = ->
  $.fn.popover.Constructor.prototype = SwitterPopover.prototype;

$(document).ready(ready);
$(document).on('page:load', ready);