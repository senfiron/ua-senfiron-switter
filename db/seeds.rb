# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Users
99.times do |n|
  email = "account-#{n+1}@example.com"
  password = 'password'
  user = User.new(email: email,
           password:              password,
           password_confirmation: password,
           confirmed_at: Time.zone.now)
  user.skip_confirmation!
  user.save!
end
users = User.all

# Avatars
users.each do |user|
  user.remote_avatar_url = Faker::Avatar.image
  user.save!
end

# Posts
rnd = Random.new
count = users.count

# 20 posts per user average
posts_count =  count * 20
posts_count.times do
  users[rnd.rand(count)].posts.create!(text: Faker::Lorem.sentence(5))
end

# Subscriptions
users.each do |user1|
  users.each do |user2|
    if user1 != user2
      # 1 to 3 chance to subscribe
      user1.subscribe(user2) if rnd.rand(3) == 0
    end
  end
end
