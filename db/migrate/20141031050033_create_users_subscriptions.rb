class CreateUsersSubscriptions < ActiveRecord::Migration
  def change
    create_table :users_subscriptions, id: false  do |t|
      t.integer :user_id
      t.integer :subscribed_to_id
    end

    add_index :users_subscriptions, :user_id
    add_index :users_subscriptions, :subscribed_to_id
    add_index :users_subscriptions, [:user_id, :subscribed_to_id], :unique => true
  end
end
