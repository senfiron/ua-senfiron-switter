require_relative '20141031050033_create_users_subscriptions'

class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions  do |t|
      t.integer :user_id
      t.integer :subscribed_to_id
      t.timestamps
    end

    add_index :subscriptions, :user_id
    add_index :subscriptions, :subscribed_to_id
    add_index :subscriptions, [:user_id, :subscribed_to_id], :unique => true

    revert CreateUsersSubscriptions
  end
end
