# “Switter” #
## Simple [Twitter](https://twitter.com) clone. ##

Демо доступно по адрессу: [ua-senfiron-switter.herokuapp.com](https://ua-senfiron-switter.herokuapp.com/)

-----

**Задание:** реализовать минимальный функционал популярной социальной сети [twitter.com](https://twitter.com)

-----

* Регистрация пользователей
    * Логин = Email
    * Пароль
    * Аватар (необязательно)
    * Captcha при регистрации (только не вот так: [http://govnokod.ru/7839](http://govnokod.ru/7839))
    * Активация аккаунта по почте
* Отправка/удаление коротких сообщений в свою ленту (не более 140 символов) с помощью AJAX. 
* Пейджинатор ленты как на [twitter.com](https://twitter.com) (на странице не более 10 сообщений, при клике на ссылку «еще», автоматически подгружать следующие 10 с помощью AJAX).
* Изменение профиля (пароля и аватарки).
* Возможность стать читателем лент других аккаунтов.
* Отображать список пользователей и их количество:
    * которых читаете Вы
    * которые читают Вас
    
-----

Для работы сайта необходимо установить значения для следующих переменных среды:

Переменная | Описание
----------------- | -------------
`MAIL_USERNAME` | Имя пользователя [Gmail](https://mail.google.com)
`MAIL_PASSWORD` | Пароль для почты [Gmail](https://mail.google.com)
`RECAPTCHA_PUBLIC_KEY` | Открытый ключ [ReCaptcha](https://www.google.com/recaptcha/intro/index.html)
`RECAPTCHA_PRIVATE_KEY` | Закрытый ключ [ReCaptcha](https://www.google.com/recaptcha/intro/index.html)
`CLOUDINARY_CLOUD_NAME` | Название облака [Cloudinary](http://cloudinary.com/)
`CLOUDINARY_API_KEY` | Ключ API [Cloudinary](http://cloudinary.com/)
`CLOUDINARY_API_SECRET` | Секретный ключ API [Cloudinary](http://cloudinary.com/)